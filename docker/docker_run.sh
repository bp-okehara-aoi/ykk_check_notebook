#!/bin/bash

GPU=0
ID=$(hostname)-${GPU//,/}-${USER//[^a-zA-Z0-9]/-}
IMAGE_ORG=ykk_work
VERSION=latest
IMAGE=gpu-registry:5000/$USER/$IMAGE_ORG:$VERSION
MEMORY=32g # 目安は実装サイズの半分としてください gpu-1050ti-[1,2,3]=8GB, gpu-1080-1=32GB, gpu-1080ti-[1,2]=64GB

docker run \
  --detach \
  --env NVIDIA_VISIBLE_DEVICES=$GPU \
  --hostname=$ID \
  --memory=$MEMORY \
  --name=$ID \
  --publish-all \
  --rm \
  --volume /home:/home \
  $IMAGE \

PORT=$(docker port $ID | grep 22/tcp | awk -F: '{print $2}')
echo ssh $USER@$(hostname) -p $PORT
